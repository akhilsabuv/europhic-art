<?php
    include 'header.php';
    include 'menu.php';
    require_once './admin/connection.php';
    require_once './admin/controller.php';
    $db_handle = new DBController();
    $datas=getProcrt($_GET['product'], $db_handle);
    $userid=$_COOKIE['userId'];
    $buycheck=buyCheck($datas['slNo'], $userid, $db_handle);
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if(array_key_exists('buynow', $_POST)){
        buyproduct($_POST['buynow'], $userid, $db_handle);
        $buycheck=buyCheck($datas['slNo'], $userid, $db_handle);
      }
    }
    $usercheck=true;
    if($_COOKIE['userId']==$datas['addedby']){
      $usercheck=false;
    }
?>

<main id="main">

<!-- ======= Intro Single ======= -->
<section class="intro-single">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="title-single-box">
          <h1 class="title-single"><?php echo $datas['name']; ?></h1>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
              <a href="property-grid.html">Products</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
              <?php echo $datas['name']; ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section><!-- End Intro Single-->

<!-- ======= Property Single ======= -->
<section class="property-single nav-arrow-b">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-8">
        <img src="<?php echo '/admin/'.$datas['photo'];?>"  height="400px"alt="">
      </div>
    </div>

    <div class="row m-4">
      <div class="col-sm-12">

        <div class="row justify-content-between">
          <div class="col-md-5 col-lg-4">
            <div class="property-price d-flex justify-content-center foo">
              <div class="card-header-c d-flex">
                <div class="card-box-ico">
                  <span class="bi bi-cash">₹</span>
                </div>
                <div class="card-title-c align-self-center">
                  <h5 class="title-c"><?php echo $datas['price']; ?></h5>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-7 col-lg-7 section-md-t3">
            <div class="row">
              <div class="col-sm-12">
                <div class="title-box-d">
                  <h3 class="title-d">Description</h3>
                </div>
              </div>
            </div>
            <div class="property-description">
              <p class="description color-text-a">
              <?php echo $datas['disc']; ?>
            </p>
            </div>
            <?php if(isset($_COOKIE['login']) && $_COOKIE['usertype']=='1' && $buycheck && $usercheck){ ?>
              <form method="post">
                <button type="submit" name="buynow" value="<?php echo $datas['slNo']; ?>" class="btn btn-primary">Buy Now</button>
              </form>
            <?php }else if($_COOKIE['usertype']=='0' && $buycheck && $usercheck){}else if($buycheck && $usercheck){ ?>
              <a href="./admin/" type="button" class="btn btn-primary">Login</a>
            <?php }else if(!$usercheck){ ?>
              <p class="text-success">Thank You for adding this product.</p>
              <?php }else{?>
              <p class="text-success">Thank You for showing an interst on the product. The owner will contact you soon.</p>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section><!-- End Property Single-->

</main><!-- End #main -->

<?php
    include 'footer.php';
?>