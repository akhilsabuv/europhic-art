-- phpMyAdmin SQL Dump
-- version 5.1.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 27, 2022 at 10:53 AM
-- Server version: 5.7.24
-- PHP Version: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `europhicart`
--

-- --------------------------------------------------------

--
-- Table structure for table `addresstable`
--

CREATE TABLE `addresstable` (
  `slNo` int(255) NOT NULL,
  `Address` varchar(2048) NOT NULL,
  `userId` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorytable`
--

CREATE TABLE `categorytable` (
  `slNo` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `disc` varchar(2048) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `producttable`
--

CREATE TABLE `producttable` (
  `slNo` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `disc` varchar(2048) NOT NULL,
  `photo` varchar(2048) NOT NULL,
  `price` varchar(1000) NOT NULL,
  `categoryNo` int(255) NOT NULL,
  `addedby` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `slNo` int(255) NOT NULL,
  `productid` int(255) NOT NULL,
  `byuserid` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usertable`
--

CREATE TABLE `usertable` (
  `slNo` int(255) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `email` varchar(1000) NOT NULL,
  `about` varchar(2048) NOT NULL,
  `phoneNo` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `type` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addresstable`
--
ALTER TABLE `addresstable`
  ADD PRIMARY KEY (`slNo`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `categorytable`
--
ALTER TABLE `categorytable`
  ADD PRIMARY KEY (`slNo`),
  ADD UNIQUE KEY `slNo` (`slNo`);

--
-- Indexes for table `producttable`
--
ALTER TABLE `producttable`
  ADD PRIMARY KEY (`slNo`),
  ADD UNIQUE KEY `slNo` (`slNo`),
  ADD KEY `addedby` (`addedby`),
  ADD KEY `categoryNo` (`categoryNo`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`slNo`),
  ADD UNIQUE KEY `slNo` (`slNo`),
  ADD KEY `byuserid` (`byuserid`);

--
-- Indexes for table `usertable`
--
ALTER TABLE `usertable`
  ADD PRIMARY KEY (`slNo`),
  ADD UNIQUE KEY `slNo` (`slNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addresstable`
--
ALTER TABLE `addresstable`
  MODIFY `slNo` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categorytable`
--
ALTER TABLE `categorytable`
  MODIFY `slNo` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `producttable`
--
ALTER TABLE `producttable`
  MODIFY `slNo` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `slNo` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usertable`
--
ALTER TABLE `usertable`
  MODIFY `slNo` int(255) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresstable`
--
ALTER TABLE `addresstable`
  ADD CONSTRAINT `addresstable_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `usertable` (`slNo`);

--
-- Constraints for table `producttable`
--
ALTER TABLE `producttable`
  ADD CONSTRAINT `producttable_ibfk_1` FOREIGN KEY (`addedby`) REFERENCES `usertable` (`slNo`),
  ADD CONSTRAINT `producttable_ibfk_2` FOREIGN KEY (`categoryNo`) REFERENCES `categorytable` (`slNo`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`byuserid`) REFERENCES `usertable` (`slNo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
