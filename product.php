<?php
    include 'header.php';
    include 'menu.php';
    require_once './admin/connection.php';
    require_once './admin/controller.php';
    $datas=getListProcrt($_GET['cat'], $db_handle);
?>

<main id="main">

<!-- ======= Intro Single ======= -->
<section class="intro-single">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-lg-8">
        <div class="title-single-box">
          <h1 class="title-single"><?php echo $datas['0']['catname'];?></h1>
          <span class="color-text-a"><?php echo $datas['0']['catdisc'];?></span>
        </div>
      </div>
      <div class="col-md-12 col-lg-4">
        <nav aria-label="breadcrumb" class="breadcrumb-box d-flex justify-content-lg-end">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">
              Our Products
            </li>
            <li class="breadcrumb-item active" aria-current="page">
              <?php echo $datas['0']['catname'];?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</section><!-- End Intro Single-->

<!-- ======= Property Grid ======= -->
<section class="property-grid grid">
  <div class="container">
    <div class="row">
      <?php foreach($datas as $data){?>
        <div class="col-md-4">
          <div class="card-box-a card-shadow">
            <div class="img-box-a">
              <img src="<?php echo '/admin/'.$data['photo'];?>" alt="" class="img-a img-fluid">
            </div>
            <div class="card-overlay">
              <div class="card-overlay-a-content">
                <div class="card-header-a">
                  <h2 class="card-title-a">
                    <a href="#"><?php echo $data['name']; ?></a>
                  </h2>
                </div>
                <div class="card-body-a">
                  <div class="price-box d-flex">
                    <span class="price-a">Price | ₹ <?php echo $data['price']; ?></span>
                  </div>
                  <a href="productsingle.php?product=<?php echo $data['slNo'];?>" class="link-a">Click here to view
                    <span class="bi bi-chevron-right"></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php }?>
    </div>
    </div>
  </div>
</section><!-- End Property Grid Single-->

</main><!-- End #main -->
<?php
    include 'footer.php';
?>