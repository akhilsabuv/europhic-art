  <?php
  require_once './admin/connection.php';
  require_once './admin/controller.php';
  $db_handle = new DBController();
  ?>
  <!-- ======= Header/Navbar ======= -->
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarDefault" aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand" href="/">
        <img src="assets/logo/logoep.png" alt="" width="50px">
        Europhic<span class="color-b">-Art</span>
    </a>

      <div class="navbar-collapse collapse justify-content-end" id="navbarDefault">
        <ul class="navbar-nav">

          <li class="nav-item">
            <a class="nav-link <?php if($_SERVER['REQUEST_URI']=='/'){ echo 'active';} ?>" href="/">Home</a>
          </li>

          <li class="nav-item">
            <a class="nav-link <?php if($_SERVER['REQUEST_URI']=='/about.php'){ echo 'active';} ?>" href="about.php">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link <?php if($_SERVER['REQUEST_URI']=='/productsingle.php' || $_SERVER['REQUEST_URI']=='/product.php'){ echo 'active';} ?> dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</a>
            <div class="dropdown-menu">
              <?php
              $tempc=getmenucategory($db_handle);
              foreach($tempc as $temp){
              ?>
              <a class="dropdown-item " href="product.php?cat=<?php echo $temp['slNo']; ?>"><?php echo $temp['name']; ?></a>
              <?php
              }
              ?>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link <?php if($_SERVER['REQUEST_URI']=='/contact.php'){ echo 'active';} ?>" href="contact.php">Contact</a>
          </li>
        </ul>
      </div>
      <?php 
      if(!isset($_COOKIE['login'])){
      ?>
        <a href="./admin/" type="button" class="btn btn-b-n pl-3">     
           <i class="bi bi-lock"></i>
            Login
      </a>
      <?php
      }else{
      ?>
        <div class="dropdown logbutton">
        <button class="btn btn-b-n pl-3  rounded-circle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <?php echo $_COOKIE['name'][0];?>    
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
          <a href="/admin/" class="dropdown-item" type="button">Dashboard</a>
          <a class="dropdown-item" href="./admin/logout.php" type="button">Logout</a>
        </div>
      <?php
      }
      ?>
</div>
        
    </div>
  </nav><!-- End Header/Navbar -->
