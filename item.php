<?php
  require_once './admin/connection.php';
  require_once './admin/controller.php';
  $db_handle = new DBController();
  $datas=getLatest($db_handle);
?>

    <!-- ======= Latest Properties Section ======= -->
    <section class="section-property section-t8">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title-wrap d-flex justify-content-between">
              <div class="title-box">
                <h2 class="title-a">Latest Properties</h2>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
        <?php foreach($datas as $data){?>
          <div class="col-md-4">
            <div class="card-box-a card-shadow">
              <div class="img-box-a">
                <img src="<?php echo '/admin/'.$data['photo'];?>" alt="" class="img-a img-fluid">
              </div>
              <div class="card-overlay">
                <div class="card-overlay-a-content">
                  <div class="card-header-a">
                    <h2 class="card-title-a">
                      <a href="#"><?php echo $data['name']; ?></a>
                    </h2>
                  </div>
                  <div class="card-body-a">
                    <div class="price-box d-flex">
                      <span class="price-a">Price | ₹ <?php echo $data['price']; ?></span>
                    </div>
                    <a href="productsingle.php?product=<?php echo $data['slNo'];?>" class="link-a">Click here to view
                      <span class="bi bi-chevron-right"></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>
      </div>
    </section><!-- End Latest Properties Section -->
