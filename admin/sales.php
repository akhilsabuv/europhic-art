<?php
	include 'header.php';
  include 'menu.php';
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $usertype=$_COOKIE['usertype'];
  $userid=$_COOKIE['userId'];
  if($usertype==0){
    $datas=getallsalesdata($db_handle);
  }else{
    $datas=getsalesdata($userid, $db_handle);
  }
?>
<main id="main" class="main">
    <section class="section dashboard">
    <!-- Recent Sales -->
    <div class="col-12">
      <div class="card recent-sales overflow-auto">

        <div class="card-body">
          <h5 class="card-title">Sales</h5>

          <table class="table table-borderless">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Customer Name</th>
                <th scope="col">Customer Email</th>
                <th scope="col">Customer Phone No</th>
                <th scope="col">Product Name</th>
                <th scope="col">Product From</th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $i=1;
                foreach($datas as $data) { ?>
              <tr>
                <th scope="row"><a><?php echo $i; ?></a></th>
                <td><?php echo $data['name']; ?></td>
                <td><a class="text-primary"><?php echo $data['email']; ?></a></td>
                <td><?php echo $data['phoneNo']; ?></td>
                <td><?php echo $data['product']; ?></td>
                <td><?php echo $data['addedName']; ?></td>                
                <td><span class="badge bg-success"><?php echo $data['price']; ?></span></td>
              </tr>
              <?php 
                $i++;
              }
              ?>
            </tbody>
          </table>

        </div>

      </div>
    </div><!-- End Recent Sales -->
    </section>
</main><!-- End #main -->

<?php
    include 'footer.php';
?>