
<?php
    require_once './connection.php';
    require_once'./controller.php';
    $db_handle = new DBController();
    $userid=$_COOKIE['userId'];
    $user = getcurrentuserdata($userid, $db_handle);
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name=$_POST['fullName'];
        $email=$_POST['email'];
        $about=$_POST['about'];
        $phoneno=$_POST['phone'];    
        if(updatecurrentuser($userid,$name,$email,$about,$phoneno,$db_handle)){
            $user=getcurrentuserdata($userid, $db_handle);
        }
    }
	include 'header.php';
    include 'menu.php';
?>

<main id="main" class="main">

<section class="section profile">
    <div class="row">
    <div class="col-xl-12">

        <div class="card">
        <div class="card-body pt-3">
            <!-- Bordered Tabs -->
            <ul class="nav nav-tabs nav-tabs-bordered">

            <li class="nav-item">
                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
            </li>

            </ul>
            <div class="tab-content pt-2">
                
            <div class="tab-pane fade show active profile-edit pt-3" id="profile-edit">

                <!-- Profile Edit Form -->
                <form method="post" action="<?php echo htmlspecialchars($_SERVER[" PHP_SELF "]);?>">
                <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                    <div class="col-md-8 col-lg-9">
                    <input name="fullName" type="text" class="form-control" id="fullName" value="<?php echo $user[0]['name'];?>">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="about" class="col-md-4 col-lg-3 col-form-label">About</label>
                    <div class="col-md-8 col-lg-9">
                    <textarea name="about" class="form-control" id="about" style="height: 100px"><?php echo $user[0]['about'];?></textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Phone" class="col-md-4 col-lg-3 col-form-label">Phone</label>
                    <div class="col-md-8 col-lg-9">
                    <input name="phone" type="text" class="form-control" id="Phone" value="<?php echo $user[0]['phoneNo'];?>">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="Email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                    <div class="col-md-8 col-lg-9">
                    <input name="email" type="email" class="form-control" id="Email" value="<?php echo $user[0]['email'];?>">
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
                </form><!-- End Profile Edit Form -->

            </div>

            </div><!-- End Bordered Tabs -->

        </div>
        </div>

    </div>
    </div>
</section>

</main><!-- End #main -->

<?php
    include 'footer.php';
?>