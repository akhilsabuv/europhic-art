<?php
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $userid=$_COOKIE['userId'];
  $usertype=$_COOKIE['usertype'];
  $datas=dashbard($userid, $usertype, $db_handle);
?>

<main id="main" class="main">

<div class="pagetitle">
  <h1>Dashboard</h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="index.html">Home</a></li>
      <li class="breadcrumb-item active">Dashboard</li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section dashboard">
  <div class="row">

    <!-- Left side columns -->
    <div class="col-lg-12">
      <div class="row">

        <!-- Sales Card -->
        <div class="col-xxl-4 col-md-6">
          <div class="card info-card sales-card">
            <div class="card-body">
              <h5 class="card-title">Sales</h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-cart"></i>
                </div>
                <div class="ps-3">
                  <h6><?php echo $datas[0]; ?></h6>
                </div>
              </div>
            </div>

          </div>
        </div><!-- End Sales Card -->

        <!-- Revenue Card -->
        <?php if($_COOKIE['usertype']=='1'){ ?>
        <div class="col-xxl-4 col-md-6">
          <div class="card info-card revenue-card">
            <div class="card-body">
              <h5 class="card-title">Orders</h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-currency-dollar"></i>
                </div>
                <div class="ps-3">
                  <h6><?php echo $datas[1]; ?></h6>
                </div>
              </div>
            </div>
          </div>
        </div><!-- End Revenue Card -->
        <?php } ?>

        <?php if($_COOKIE['usertype']=='0'){ ?>
        <!-- Customers Card -->
        <div class="col-xxl-4 col-xl-12">

          <div class="card info-card customers-card">
            <div class="card-body">
              <h5 class="card-title">Customers</h5>

              <div class="d-flex align-items-center">
                <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                  <i class="bi bi-people"></i>
                </div>
                <div class="ps-3">
                  <h6><?php echo $datas[1]; ?></h6>
                </div>
              </div>

            </div>
          </div>

        </div><!-- End Customers Card -->
        <?php }?>
      </div>
    </div><!-- End Left side columns -->
  </div>
</section>
</main><!-- End #main -->
