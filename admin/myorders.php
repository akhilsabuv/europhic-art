<?php
	include 'header.php';
  include 'menu.php';
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $userid=$_COOKIE['userId'];
  $datas=getorderdata($userid, $db_handle);
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(array_key_exists('cancelorder', $_POST)){
      cancelorder($_POST['cancelorder'], $db_handle);
      $datas=getorderdata($userid, $db_handle);
    }
  }

?>
<main id="main" class="main">
    <section class="section dashboard">
    <!-- Recent Sales -->
    <div class="col-12">
      <div class="card recent-sales overflow-auto">
        <div class="card-body">
          <h5 class="card-title">My Orders</h5>

          <table class="table table-borderless">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Product From</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $i=1;
                foreach($datas as $data){?>
              <tr>
                <th scope="row"><a>#<?php echo $i; ?></a></th>
                <td><a class="text-primary"><?php echo $data['pname']; ?></a></td>
                <td><?php echo $data['price']; ?></td>
                <td><?php echo $data['name']; ?></td>
                <td>
                  <form method="post">
                    <button type="submit" name="cancelorder" value="<?php echo $data['slNo']; ?>" class="badge bg-danger">Cancel Order</button>
                  </form>
                </td>
              </tr>
              <?php $i++; } ?>
            </tbody>
          </table>
        </div>

      </div>
    </div><!-- End Recent Sales -->
    </section>
</main><!-- End #main -->

<?php
    include 'footer.php';
?>