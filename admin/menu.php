<?php
if (!isset($_COOKIE['login'])) {
  header("Location: login.php");      
}

?>
  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">
    <div class="d-flex align-items-center justify-content-between">
      <a href="/" class="logo d-flex align-items-center">
        <img src="./../assets/logo/logoep.png" alt="">
        <span class="d-none d-lg-block">Europhic Art</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->
    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">
        <li class="nav-item dropdown pe-3">
          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
          <button type="button" class="btn btn-b-n pl-3 bcbutton rounded-circle">     
            <?php echo $_COOKIE['name'][0];?>    
          </button>
          <span class="d-none d-md-block dropdown-toggle ps-2"><?php echo $_COOKIE['name'];?></span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
              <h6><?php echo $_COOKIE['name'];?></h6>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="profile.php">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>
            <li>
              <form method="post" action= "logout.php">
                <button class="dropdown-item d-flex align-items-center" name="logout" id="logout" value="logout" type="submit">
                  <i class="bi bi-box-arrow-right"></i>
                  <span>Sign Out</span>
                </button>
              </form>
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">
    <ul class="sidebar-nav" id="sidebar-nav">
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/index.php'){ echo 'collapsed';} ?>" href="/admin/index.php">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->
      <?php if($_COOKIE['usertype']=='0'){ ?>
      <li class="nav-heading">Admin</li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/products.php'){ echo 'collapsed';} ?>" href="products.php">
          <i class="bi bi-archive-fill"></i>
          <span>Products</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/sales.php'){ echo 'collapsed';} ?>" href="sales.php">
          <i class="bi bi-cart"></i>
          <span>Sales</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/users.php'){ echo 'collapsed';} ?>" href="users.php">
        <i class="bi bi-diagram-3"></i>
          <span>Users</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/category.php'){ echo 'collapsed';} ?>" href="category.php">
        <i class="bi bi-diagram-3"></i>
          <span>Category</span>
        </a>
      </li>
      <?php }else{ ?>
      <li class="nav-heading">Users</li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/products.php'){ echo 'collapsed';} ?>" href="products.php">
        <i class="bi bi-archive-fill"></i>
          <span>Products</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/sales.php'){ echo 'collapsed';} ?>" href="sales.php">
        <i class="bi bi-cart"></i>
          <span>Sales</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($_SERVER['REQUEST_URI']!='/admin/myorders.php'){ echo 'collapsed';} ?>" href="myorders.php">
        <i class="bi bi-card-heading"></i>
        <span>My Orders</span>
        </a>
      </li>
      <?php } ?>
    </ul>

  </aside><!-- End Sidebar-->