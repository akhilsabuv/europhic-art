<?php
  require_once './connection.php';
  require_once'./controller.php';
  $db_handle = new DBController();
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name=$_POST['name'];
    $email=$_POST['email'];
    $about="";
    $phoneno=$_POST['number'];
    $password=md5($_POST['password']);
    if(signup($name,$email,$about,$phoneno,$password,$db_handle)){
      header("Location: login.php");
    }
  }

  include 'header.php';
?>
  <main>
    <div class="container">
      <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

              <div class="d-flex justify-content-center py-4">
                <a href="/" class="logo d-flex align-items-center w-auto">
                <img src="./../assets/logo/logoep.png" alt="">
                <span class="d-none d-lg-block">Europhic Art</span>
                </a>
              </div><!-- End Logo -->

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <h5 class="card-title text-center pb-0 fs-4">Create an Account</h5>
                    <p class="text-center small">Enter your personal details to create account</p>
                  </div>

                  <form method="post" action= "<?php echo htmlspecialchars($_SERVER[" PHP_SELF "]);?>" class="row g-3 needs-validation" novalidate>
                    <div class="col-12">
                      <label for="yourName" class="form-label">Your Name</label>
                      <input type="text" name="name" class="form-control" id="yourName" required>
                      <div class="invalid-feedback">Please, enter your name!</div>
                    </div>

                    <div class="col-12">
                      <label for="yourEmail" class="form-label">Your Email</label>
                      <input type="email" name="email" class="form-control" id="yourEmail" required>
                      <div class="invalid-feedback">Please enter a valid Email adddress!</div>
                    </div>

                    <div class="col-12">
                      <label for="yourEmail" class="form-label">Phone Number</label>
                      <input type="text" name="number" class="form-control" id="number" required>
                      <div class="invalid-feedback">Please enter a valid Phone Number!</div>
                    </div>


                    <div class="col-12">
                      <label for="yourPassword" class="form-label">Password</label>
                      <input type="password" name="password" class="form-control" id="yourPassword" required>
                      <div class="invalid-feedback">Please enter your password!</div>
                    </div>

                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit" value="Submit" name="submit">Create Account</button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Already have an account? <a href="login.php">Log in</a></p>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->
<?php
  include 'footer.php';
?>