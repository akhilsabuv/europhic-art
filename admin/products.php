<?php
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $addedby=$_COOKIE['userId'];
  $usertype=$_COOKIE['usertype'];
  $catagory=getcategory($db_handle);
  $products=getproductsmy($addedby, $usertype ,$db_handle);

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(array_key_exists('addproduct', $_POST)){
      $name = $_POST['pname'];
      $disc = $_POST['pdisc'];
      $price = $_POST['pprice'];
      $cat = $_POST['pcat'];
      if(is_array($_FILES)) {
        $photo = addFile();
      }
      if(addProduct($name,$disc,$photo,$price,$cat,$addedby,$db_handle)){
        $products=getproductsmy($addedby, $usertype ,$db_handle);
      }  
    }
    if(array_key_exists('remove', $_POST)){
      if(removeproduct($usertype, $_POST['remove'], $_POST['image'], $db_handle)){
        $products=getproductsmy($addedby, $usertype ,$db_handle);
      }
    }
    if(array_key_exists('approve', $_POST)){
      if(approveproduct($usertype, $_POST['approve'], $db_handle)){
        $products=getproductsmy($addedby, $usertype ,$db_handle);
      }
    }
    if(array_key_exists('reject', $_POST)){
      if(removeproduct($usertype, $_POST['reject'], $_POST['image'], $db_handle)){
        $products=getproductsmy($addedby, $usertype ,$db_handle);
      }
    }  
  }
  
	include 'header.php';
  include 'menu.php';
?>
<main id="main" class="main">
    <section class="section dashboard">
    <!-- Recent Sales -->
    <div class="col-12">
          <div class="card top-selling overflow-auto">
          <?php if($usertype!=0){?>
            <div class="filter">
              <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
              <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#basicModal">Add Product</a></li>
              </ul>
            </div>
          <?php } ?>
            <div class="card-body pb-0">
              <h5 class="card-title">Products</h5>

              <table class="table table-borderless">
                <thead>
                  <tr>
                    <th scope="col">Preview</th>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Discription</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($products as $product){?>
                  <tr>
                    <th scope="row"><a href="#"><img src="/admin/<?php echo $product['photo'];?>" alt=""></a></th>
                    <td><?php echo $product['name'];?></td>
                    <td class="fw-bold"><?php echo $product['price'];?></td>
                    <td><a class="text-primary"><?php echo $product['disc'];?></a></td>
                    <td>
                    <?php if($usertype!=0){?>
                    <form method="post">
                      <?php if($product['approved']){ ?>
                        <input type="hidden" name="image" value="<?php echo $product['photo'];?>">
                        <button type="submit" name="remove" value="<?php echo $product['slNo']; ?>" class="badge bg-danger">Remove</button>
                      <?php }else{ ?>
                        <span class="badge bg-success">Waiting For Approval</span>
                      <?php } ?>
                    </form>
                    <?php }else{ ?>
                      <?php if(!$product['approved']){ ?>
                      <form method="post" class="badge">
                        <input type="hidden" name="image" value="<?php echo $product['photo'];?>">
                        <button type="submit" name="reject" value="<?php echo $product['slNo']; ?>" class="badge bg-danger">Reject</button>
                      </form>
                      <form method="post" class="badge">
                        <button type="submit" name="approve" value="<?php echo $product['slNo']; ?>" class="badge bg-success">Approve</button>
                      </form>
                      <?php }else{ ?>
                      <span class="badge bg-success">Approved</span>
                      <?php } ?>
                      <?php } ?>
                    </td>
                  </tr>
                  <?php }?>
                </tbody>
              </table>

            </div>

          </div>
        </div><!-- End Top Selling -->
    </div><!-- End Recent Sales -->
    </section>
</main><!-- End #main -->

<!-- Basic Modal -->
<div class="modal fade" id="basicModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Product</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form method="post"  enctype="multipart/form-data" action= "" class="row g-3 needs-validation" novalidate>
          <div class="col-12">
            <label for="inputNanme4" class="form-label">Product Name</label>
            <input type="text" name="pname" class="form-control" id="inputNanme4">
          </div>
          <div class="col-12">
            <label for="inputNumber" class="col-sm-12 col-form-label">Product Photo</label>
            <div class="col-sm-12">
              <input class="form-control" name="image" type="file" id="image">
            </div>
          </div>
          <div class="col-12">
            <label for="inputEmail4" class="form-label">Discription</label>
            <input type="text" name="pdisc" class="form-control" id="inputEmail4">
          </div>
          <div class="col-12">
            <label for="inputEmail4" class="form-label">Price</label>
            <input type="number" name="pprice" class="form-control" id="inputEmail4">
          </div>
          <div class="col-12">
              <label class="col-sm-12 col-form-label">Select Category</label>
              <div class="col-sm-12">
                <select class="form-select" name="pcat" aria-label="Default select example">
                <?php foreach($catagory as $cat){ ?>
                  <option value="<?php echo $cat['slNo']; ?>"><?php echo $cat['name']; ?></option>
                <?php } ?>
                </select>
              </div>
            </div>
          <div class="text-center">
            <button type="submit" name="addproduct" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
          </div>
        </form><!-- Vertical Form -->
      </div>
    </div>
  </div>
</div><!-- End Basic Modal-->

<?php
    include 'footer.php';
?>