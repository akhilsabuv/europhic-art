<?php
function getUsers($db_handle){
    $sql="SELECT `slNo`, `name`, `email`, `about`, `phoneNo`, `type` FROM `usertable` WHERE 1";
    $data=$db_handle->runQuery($sql);    
    if($data){
        return $data;
    }else{
        return [];
    }
}
//add product
function addProduct($name,$disc,$photo,$price,$cat,$addedby,$db_handle){
    $sql="INSERT INTO `producttable`(`name`, `disc`, `photo`, `price`, `categoryNo`, `addedby`) VALUES ('$name','$disc','$photo','$price','$cat','$addedby')";
    $db_handle->runInsertQuery($sql);    
    return true;
}
// add address
function addAddress($address,$userid,$db_handle){
    $sql="INSERT INTO `addresstable`(`Address`, `userId`) VALUES ('$address','$userid')";
    return $db_handle->runInsertQuery($sql);    
}
// add address
function addSales($product, $byuser,$db_handle){
    $sql="INSERT INTO `sales`(`productid`, `byuserid`) VALUES ('$product','$byuser')";
    return $db_handle->runInsertQuery($sql);    
}


// login
function login($username,$password,$db_handle){
    $sql="SELECT * FROM `usertable` WHERE `email`='$username' AND `password`='$password'";
    $msg = $db_handle->runQuery($sql);    
    if($msg){
        setCookieAll('login','active');
        setCookieAll('usertype',$msg[0]['type']);
        setCookieAll('name',$msg[0]['name']);
        setCookieAll('userId',$msg[0]['slNo']);
        return true;  
    }else{
        unset($_COOKIE['usertype']);  
        unset($_COOKIE['login']);  
        return false;
    }
}

// signup
function signup($name,$email,$about,$phoneno,$password,$db_handle){
    $sql="INSERT INTO `usertable`(`name`, `email`, `about`, `phoneNo`, `password`, `type`) VALUES ('$name','$email','$about','$phoneno','$password','1')";
    $db_handle->runInsertQuery($sql);
    return true;    
}
// userdata get
function getcurrentuserdata($userid, $db_handle){
    $sql="SELECT `name`, `email`, `about`, `phoneNo` FROM `usertable` WHERE `slNo`='$userid'";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}

function getproductsmy($addedby, $usertype ,$db_handle){
    if($usertype==0){
        $sql="SELECT * FROM `producttable` WHERE 1";
        $data=$db_handle->runQuery($sql);   
        if($data){
            return $data;
        }else{
            return [];
        }     
    }else{
        $sql="SELECT * FROM `producttable` WHERE `addedby`='$addedby'";
        $data=$db_handle->runQuery($sql);   
        if($data){
            return $data;
        }else{
            return [];
        }
    }
}

function removeproduct($usertype, $id, $image, $db_handle){
    unlink($image);
    $sql="DELETE FROM `producttable` WHERE `slNo`='$id'";
    return $db_handle->rnDeleteQuery($sql);        
}

function approveproduct($usertype, $id, $db_handle){
    if($usertype==0){
        $sql="UPDATE `producttable` SET `approved`='1' WHERE `slNo`='$id'";
        return $db_handle->rnDeleteQuery($sql);        
    }
}

function updatecurrentuser($userid,$name,$email,$about,$phoneno,$db_handle){
    $sql="UPDATE `usertable` SET `name`='$name',`email`='$email',`about`='$about',`phoneNo`='$phoneno' WHERE `slNo`='$userid'";
    $db_handle->rnUpdateQuery($sql);
    return true;
}

function addcategory($category, $disc,$db_handle){
    $sql="INSERT INTO `categorytable`(`name`, `disc`) VALUES ('$category','$disc')";
    $db_handle->runInsertQuery($sql);
    return true;   
}

function getcategory($db_handle){
    $sql="SELECT * FROM `categorytable` WHERE 1";
    $data=$db_handle->runQuery($sql);    
    if($data){
        return $data;
    }else{
        return [];
    }
}


function getmenucategory($db_handle){
    $sql="SELECT * FROM `categorytable` WHERE 1";
    $data=$db_handle->runQuery($sql);
    $array=[];
    foreach($data as $d){
        $dax=$d['slNo'];
        $check="SELECT count(*) as c FROM `producttable` WHERE `categoryNo`='$dax' AND `approved`='1'";
        $checkdata=$db_handle->runQuery($check);
        if($checkdata[0]['c']>0){
            array_push($array, $d);
        }
    }
    return $array;
}


function removecategory($usertype, $id, $db_handle){
    if($usertype==0){
        $sql="DELETE FROM `categorytable` WHERE `slNo`='$id'";
        return $db_handle->rnDeleteQuery($sql);        
    }
}


function setCookieAll($name,$value){
    setcookie("$name", "$value", time() + (86400 * 30), "/");
}

function getCookieAll($name){
    return $_COOKIE['$name'];
}

function addFile(){
    if(isset($_FILES['image'])){
        $errors= array();
        $file_name = $_FILES['image']['name'];
        $file_size =$_FILES['image']['size'];
        $file_tmp =$_FILES['image']['tmp_name'];
        $file_type=$_FILES['image']['type'];
        $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
        
        $extensions= array("jpeg","jpg","png");
        
        if(in_array($file_ext,$extensions)=== false){
           $errors[]="extension not allowed, please choose a JPEG or PNG file.";
        }
                
        if(empty($errors)==true){
           move_uploaded_file($file_tmp,"images/".$file_name);
           return("images/".$file_name);
        }else{
           print_r($errors);
        }
     }
}
function getLatest($db_handle){
    $sql="SELECT * FROM `producttable` WHERE `approved`='1' LIMIT 3 ";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}

function getListProcrt($id, $db_handle){
    $sql="SELECT `producttable`.`slNo`, `producttable`.`name`, `producttable`.`disc`, `photo`, `price`, `categoryNo`, `addedby`, `approved`, `categorytable`.`name` as catname, `categorytable`.`disc` as catdisc FROM `producttable` Left Join `categorytable` ON `categorytable`.`slNo`=`producttable`.`categoryNo` WHERE `categoryNo`='$id' AND `approved`='1';";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}

function getProcrt($id, $db_handle){
    $sql="SELECT `producttable`.`slNo`, `producttable`.`name`, `producttable`.`disc`, `photo`, `price`, `categoryNo`, `addedby`, `approved`, `categorytable`.`name` as catname, `categorytable`.`disc` as catdisc FROM `producttable` Left Join `categorytable` ON `categorytable`.`slNo`=`producttable`.`categoryNo` WHERE `producttable`.`slNo`='$id';";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data[0];
    }else{
        return '';
    }}

// buy product

function buyproduct($id, $user, $db_handle){
    $sql="INSERT INTO `sales`(`productid`, `byuserid`) VALUES ('$id','$user');";
    $db_handle->runInsertQuery($sql);
}

function buyCheck($id, $user, $db_handle){
    $sql="SELECT * FROM `sales` WHERE `byuserid`='$user' AND `productid`='$id';";
    $data=$db_handle->runQuery($sql);
    if($data){
        return false;
    }else{
        return true;
    }
}

//sales data
function getallsalesdata($db_handle){
    $sql="SELECT B.name ,B.email ,B.phoneNo ,B.product ,B.price ,`usertable`.`name` AS addedName FROM (( SELECT A.* ,`producttable`.`name` AS product ,`producttable`.`addedby` ,`producttable`.`price` FROM (( SELECT `sales`.* ,`usertable`.`name` ,`usertable`.`email` ,`usertable`.`phoneNo` FROM `sales` INNER JOIN `usertable` ON `usertable`.`slNo` = `byuserid`)A ) LEFT JOIN `producttable` ON A.`productid` = `producttable`.`slNo`)B ) LEFT JOIN `usertable` ON `usertable`.`slNo` = B.`addedby`;";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}
function getsalesdata($userid, $db_handle){
    $sql="SELECT B.id ,B.name ,B.email ,B.phoneNo ,B.product ,B.price ,`usertable`.`name` AS addedName FROM (( SELECT A.* ,`producttable`.`name` AS product ,`producttable`.`addedby` ,`producttable`.`price` FROM (( SELECT `sales`.* ,`usertable`.`slNo` as id ,`usertable`.`name` ,`usertable`.`email` ,`usertable`.`phoneNo` FROM `sales` INNER JOIN `usertable` ON `usertable`.`slNo` = `byuserid`)A ) LEFT JOIN `producttable` ON A.`productid` = `producttable`.`slNo`)B ) LEFT JOIN `usertable` ON `usertable`.`slNo` = B.`addedby` WHERE `usertable`.`slNo`='$userid';";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}

//order data
function getorderdata($userid, $db_handle){
    $sql="SELECT A.*, `usertable`.`name` FROM((SELECT `sales`.*, `producttable`.`name`  AS pname,`producttable`.`price` FROM `sales` INNER JOIN `producttable` ON `sales`.`productid`=`producttable`.`slNo`)A)LEFT JOIN `usertable` ON A.`byuserid`=`usertable`.`slNo` WHERE `usertable`.`slNo`='$userid';";
    $data=$db_handle->runQuery($sql);
    if($data){
        return $data;
    }else{
        return [];
    }
}

function cancelorder($id, $db_handle){
    $sql="DELETE FROM `sales` WHERE `slNo`='$id'";
    return $db_handle->rnDeleteQuery($sql);        
}

// dashboard
function dashbard($id, $usertype, $db_handle){
    $final=[];
    if($usertype==0){
        $sql="SELECT COUNT(*) AS sales FROM `sales`";
        $sql1="SELECT COUNT(*) AS users FROM `usertable` WHERE `type`='1'";
        $data=$db_handle->runQuery($sql);
        $data1=$db_handle->runQuery($sql1);
        array_push($final, $data[0]['sales'], $data1[0]['users']);
    }else{
        $sql="SELECT COUNT(*) AS sales FROM `sales` INNER JOIN `producttable` ON `sales`.`productid`=`producttable`.`slNo` WHERE `sales`.`byuserid`='$id'";
        $sql1="SELECT COUNT(*) AS od FROM `sales` INNER JOIN `producttable` ON `sales`.`productid`=`producttable`.`slNo` WHERE `producttable`.`addedby`='$id'";    
        $data=$db_handle->runQuery($sql);
        $data1=$db_handle->runQuery($sql1);
        array_push($final, $data[0]['sales'], $data1[0]['od']);
    }
    return $final;
}
?>