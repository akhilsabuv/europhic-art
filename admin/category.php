<?php
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $catagory=getcategory($db_handle);
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(array_key_exists('remove', $_POST)){
      if(removecategory($usertype, $_POST['remove'], $db_handle)){
        $catagory=getcategory($db_handle);
      }
    }else{
      $category=$_POST['cat'];
      $disc=$_POST['disc'];
      if(addcategory($category, $disc, $db_handle)){
        $catagory=getcategory($db_handle);
      }
    }
  }


  
	include 'header.php';
  include 'menu.php';
?>
<main id="main" class="main">
    <section class="section dashboard">
    <!-- Recent Sales -->
    <div class="col-12">
      <div class="card recent-sales overflow-auto">
        <div class="filter">
          <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#basicModal">Add Category</a></li>
          </ul>
        </div>

        <div class="card-body">
          <h5 class="card-title">Category</h5>

          <table class="table table-borderless">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Category</th>
                <th scope="col">Discription</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
            <?php 
              $index=1;
              foreach($catagory as $cat){
            ?>
              <tr>
                <th scope="row"><a><?php echo $index;?></a></th>
                <th scope="row"><a><?php echo $cat['name'];?></a></th>
                <td><?php echo $cat['disc'];?></td>
                <td>
                  <form method="post">
                      <button type="submit" name="remove" value="<?php echo $cat['slNo']; ?>" class="badge bg-danger">Remove</button>
                    </form>
                </td>
              </tr>
            <?php $index++; }?>
            </tbody>
          </table>

        </div>

      </div>
    </div><!-- End Recent Sales -->
    </section>
</main><!-- End #main -->

<!-- Basic Modal -->
<div class="modal fade" id="basicModal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Category</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action= "<?php echo htmlspecialchars($_SERVER[" PHP_SELF "]);?>" class="row g-3 needs-validation" novalidate>
          <div class="col-12">
            <label for="inputAddress" class="form-label">Category</label>
            <input type="text" class="form-control" id="category" name="cat" placeholder="1234 Main St">
          </div>
          <div class="col-12">
            <label for="inputAddress" class="form-label">Discription</label>
            <textarea type="text" class="form-control" id="discription" name="disc" placeholder="1234 Main St"></textarea>
          </div>
          <div class="text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
          </div>
        </form><!-- Vertical Form -->
      </div>
    </div>
  </div>
</div><!-- End Basic Modal-->


<?php
    include 'footer.php';
?>