<?php
  require_once './connection.php';
  require_once './controller.php';
  $db_handle = new DBController();
  $addedby=$_COOKIE['userId'];
  $usertype=$_COOKIE['usertype'];

  $users=getUsers($db_handle);

	include 'header.php';
  include 'menu.php';
?>
    <main id="main" class="main">
    <section class="section dashboard">
    <!-- Recent Sales -->
    <div class="col-12">
      <div class="card recent-sales overflow-auto">
        <div class="card-body">
          <h5 class="card-title">Users</h5>

          <table class="table table-borderless">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Users</th>
                <th scope="col">About</th>
                <th scope="col">Email</th>
                <th scope="col">Mobile</th>
                <th scope="col">Type</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $index=1;
                foreach($users as $user){
              ?>
              <tr>
                <th scope="row"><a href="#"><?php echo $index; ?></a></th>
                <td><?php echo $user['name']; ?></td>
                <td><a class="text-primary"><?php echo $user['about']; ?></a></td>
                <td><?php echo $user['email']; ?></td>
                <td><?php echo $user['phoneNo']; ?></td>
                <?php if($user['type']==0){ ?>
                  <td>Admin</td>
                <?php }else{ ?>
                  <td>User</td>
                <?php } ?>
              </tr>
              <?php
                $index++;
                } 
              ?>
            </tbody>
          </table>

        </div>

      </div>
    </div><!-- End Recent Sales -->
    </section>
</main><!-- End #main -->
<?php
    include 'footer.php';
?>